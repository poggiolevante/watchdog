const uint32_t th = 60000;        // tempo soglia o watchdog timer (milliseconds) (di prova sono 2 sec = 2000 ms)
const uint32_t bootDelay = 60000;  // boot delay (milliseconds) (di prova sono 5 se = 5000 ms)
uint64_t last_ping;

#define watchdog_in 1     // Pin che riceverà l'impulso
#define watchdog_out 4    // Pin che attiverà il reset HW del sistema
#define led_boot_delay 3  // Pin che attiva il led integrato del microcontrollore

void setup() {
  pinMode(watchdog_in, INPUT);
  pinMode(watchdog_out, OUTPUT);
  pinMode(led_boot_delay, OUTPUT);
  digitalWrite(watchdog_out, HIGH);  //La linea di default è settata su HIGH, per effettuare il reset bisogna settarla su LOW
  digitalWrite(led_boot_delay, LOW);
}

// the loop function runs over and over again forever
void loop() {
  digitalWrite(led_boot_delay, HIGH);
  delay(bootDelay);  //Fase di inizializzazione
  digitalWrite(led_boot_delay, LOW);
  last_ping = millis();
  do {
    if (digitalRead(watchdog_in) == HIGH) {
      last_ping = millis();  //Loop di verifica di funzionamento del sistema
      digitalWrite(led_boot_delay, HIGH);
    } else digitalWrite(led_boot_delay, LOW);
  } while (millis() - last_ping <= th);
  digitalWrite(watchdog_out, LOW);
  delay(200);  //Fase di RESET, con il pin RESET settato su LOW
  digitalWrite(watchdog_out, HIGH);
}

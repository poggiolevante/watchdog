const uint32_t th = 10000;  //milliseconds
const uint32_t bootDelay = 5000; //milliseconds
uint64_t last_ping;

#define watchdog_in 1
#define watchdog_out 3

void setup() {
  pinMode(watchdog_in, INPUT);
  pinMode(watchdog_out, OUTPUT);
  digitalWrite(watchdog_out, HIGH);
}

// the loop function runs over and over again forever
void loop() {
  delay(bootDelay);
  last_ping = millis();
  do{
    if (digitalRead(watchdog_in) == HIGH ) last_ping = millis();
  }while(millis() - last_ping < th);
  digitalWrite(watchdog_out, LOW);
  delay(5);
  digitalWrite(watchdog_out, HIGH);
}

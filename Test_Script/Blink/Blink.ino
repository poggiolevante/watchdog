const uint32_t th = 10000;  //milliseconds
const uint32_t bootDelay = 5000; //milliseconds
uint64_t last_ping;

#define watchdog_in 1
#define watchdog_out 3

void setup() {
  pinMode(watchdog_in, OUTPUT);
  pinMode(watchdog_out, OUTPUT);
}

// the loop function runs over and over again forever
void loop() {
  digitalWrite(1, HIGH);
  delay(1000);
  digitalWrite(1,LOW);
  delay(1000);
}

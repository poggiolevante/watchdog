Un piccolo sistema costituito da un **microcontrollore** che rileva un malfunzionamento hardware o software di un altro sistema (preferibilmente un altro microcontrollore o comunque un qualsiasi tipo di scheda elettronica programmabile fornita di PIN digitali per l'input o l'output) e **resetta** quest'ultimo affinché possa ricominciare a funzionare.

![Watchdog](https://gitlab.com/poggiolevante/watchdog/-/raw/main/Require/Resources/watchdog.jpg)

# Specifiche del progetto:
L'idea del progetto è nata a causa di malfunzionamento di alcuni termoigrometri WiFi. I termoigrometri effettuano misurazioni sull'ambiente circostante e riportano quest'ultime su dei fogli Google ogni quantum di tempo. Tuttavia con il passare del tempo alcuni termoigrometri smettono di funzionare a causa di problemi derivanti dalle operazioni di scrittura sul foglio, provocando un **deadlock** con conseguente blocco del sistema. Per questo abbiamo pensato a un **temporizzatore di supervisione** facile da creare e implementare.

## Come funziona?
Il **WatchDog**, attraverso un **PIN** digitale settato in **INPUT**, rimane in attesa ciclicamente di un segnale da parte del sistema da supervisionare, per verificare che quest'ultimo sia funzionante. Quando il sistema smetterà di funzionare, non invierà più il segnale al **WatchDog**, di conseguenza esso dopo un tempo **treeshold** manderà un segnale **basso** alla linea di **RESET**, attraverso un **PIN** digitale settato in **OUTPUT**. Riavviando cosi il sistema.

## Open Source e Documentazione:
Questo progetto è open source: chiunque può scaricare i file necessari, ricreare il progetto e contribuire al suo miglioramento. Non ci sono restrizioni di licenza d'uso, ma si invita a citare che è stato realizzato dagli studenti [ASIRID.](http://asirid.it)

Tutto il materiale necessario si trova [su GitLab.](https://gitlab.com/poggiolevante/watchdog)

## Requisiti HW:
 - **ATTiny85**
 >Oppure qualsiasi tipo di microcontrollore con pin digitali (che sia programmabile con l'**IDE Arduino**)
 - 4 **Jumper** per i collegamenti
 - 2 **PIN** digitali sul **WatchDog** per **INPUT** e **OUTPUT**
 - 1 **PIN** digitale sul **sistema da supervisionare**
 -  **PIN** di **RESET** sul **sistema da supervisionare**
 - 2 **PIN** per l'**alimentazione** e per la **messa a terra** (sia sul WatchDog che sul sistema da supervisionare)
 >**Topologia:**
![Schema ](https://gitlab.com/poggiolevante/watchdog/-/raw/main/Require/Resources/Topologia.png)
 
## Requisiti SW:
-Codice **.ino** reperibile su questa [**repository GitLab**](https://gitlab.com/poggiolevante/watchdog) 
>La **repository** è cosi composta:
![DirTree](https://gitlab.com/poggiolevante/watchdog/-/raw/main/Require/Resources/Dir_Struct_watchdog.png)
>- **Require**: al suo interno sono riposte tutte le risorse utili al funzionamento del Watchdog.
>- **Test_Script**: sono presenti codici per testare il funzionamento l'ATTiny.
>- **watchdog_0.1**: il programma .ino da caricare sull'ATTiny per implementare il WatchDog.

## Passi da seguire per l'implementazione:
>Guida per ATTiny85, nel caso di altre schede e/o microcontrollori installare le dovute dipendenze.
 1. Effettuare i collegamenti sulle schede

 2. Aprire Arduino IDE **come amministratore**

 3. Installare i **driver** della scheda elettronica programmabile designata al compito di WatchDog. Nel caso di ATTiny85 i driver sono situati nella cartella ./Require/Digistump.Drivers.zip

 4. Aggiungere l'url aggiuntivo presente su ./Require/URL_json_board.txt andando su **File** -> **Impostazioni**:

![url_json](https://gitlab.com/poggiolevante/watchdog/-/raw/main/Require/Resources/add_board__json_method.png)

 5. Installare **Digistump AVR Boards** attraverso il Gestore Schede di Arduino e selezionarlo tramite **Strumenti** -> **Scheda** -> **Gestore Scheda**:

![steps_install_driver](https://gitlab.com/poggiolevante/watchdog/-/raw/main/Require/Resources/board_driver.png)
![digistump_packet](https://gitlab.com/poggiolevante/watchdog/-/raw/main/Require/Resources/digstump_packet.png)

 6. Selezionare la scheda in **Strumenti** -> **Scheda** -> **Digistump AVR Boards** -> **Digispark (Default - 16.5mhz)**
 
 7. Aprire **watchdog_0.1.ino**, premere il tasto "**Carica**" e aspettare che la console stampi questo messaggio: "**Plug in device now...**".

 8. Collegare l'ATTiny85 e aspettare la fine del caricamento dello sketch sulla board.

## Costanti da modificare nello sketch:
 ```cpp
const uint32_t th = 60000;  //tempo soglia o watchdog timer (milliseconds)
const uint32_t bootDelay = 60000; //boot delay (milliseconds)

#define watchdog_in 1 //Pin che riceverà l'impulso 
#define watchdog_out 4 //Pin che attiverà il reset HW del sistema
#define led_boot_delay 3 //Pin che attiva il led integrato del microcontrollore
```
> Modificare le costanti per:
> - **th** -> tempo massimo di attesa prima che il Watchdog attivi il segnale di RESET (nel caso non gli arrivi più nessun segnale in input).
> - **bootDelay** -> tempo di accensione prima che il WatchDog cominci il loop di verifica.
> - **watchdog_in** -> PIN che riceverà ciclicamente il segnale dal sistema da supervisionare.
> - **watchdog_out** -> PIN che attiverà il RESET.

## Stato attuale del progetto:
Il progetto è stato pubblicato a Marzo 2022, nella sua prima versione la 0.1 ed è attualmente funzionante.
Sviluppi futuri da definire.

## Autore:
Tommaso Orlando, Aprile 2022
